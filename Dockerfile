FROM python:3.9-buster

COPY pyproject.toml /src/
ADD py_whisper_worker /src/py_whisper_worker
WORKDIR /src

RUN apt-get update && \
    apt-get install ffmpeg -y

RUN pip install .

ENV AMQP_QUEUE=job_whisper_transcript
ENV MCAI_LOG=info
ENV WHISPE_MODEL=medium

CMD py_whisper_worker 
