# py_whisper_worker

A Python worker based on [`Whisper`](https://github.com/openai/whisper).

## Configuration

- The model used can be configured through the environment variable `WHISPER_MODEL`. Default to `medium`.
- The output format is configured is the source ordre. It must be one of the format supported by [`openai-whisper`](https://github.com/openai/whisper/blob/main/whisper/transcribe.py#LL384C23-L384C23).
