import logging
import os
import sys

import mcai_worker_sdk as mcai
import tqdm

import whisper
import whisper.transcribe
from whisper.utils import get_writer


class McaiWorkerParameters(mcai.WorkerParameters):
    source_path: str
    destination_path: str
    output_format: str


class McaiWorker(mcai.Worker):
    model = None
    _patched = False
    _channel = None

    def _patch_progress_bar(self, channel: mcai.McaiChannel):
        self._channel = channel

        if self._patched:
            return None

        class _McaiProgressBar(tqdm.tqdm):
            _worker = self

            def __init__(self, *args, **kwargs):
                self._current = 0
                super().__init__(*args, **kwargs)

            def update(self, n):
                super().update(n)
                self._current += n

                self._worker._channel.publish_job_progression(
                    int(100 * self._current / self.total)
                )

        transcribe_module = sys.modules["whisper.transcribe"]
        transcribe_module.tqdm.tqdm = _McaiProgressBar
        self._patched = True

    def setup(self) -> None:
        model_size = os.environ.get("WHISPER_MODEL", "medium")
        if model_size not in whisper.available_models():
            logging.error(
                "Specified model %s is not available in whisper. Fallback to medium.",
                model_size,
            )
            model_size = "medium"
        self.model = whisper.load_model(model_size)
        logging.info("Whisper model loaded")

    def process(
        self, channel: mcai.McaiChannel, parameters: McaiWorkerParameters, _job_id: int
    ):
        self._patch_progress_bar(channel)
        logging.info("Start audio transcription.")
        transcript = self.model.transcribe(parameters.source_path)

        logging.info(
            "Writing generated subtitles with format %s into %s.",
            parameters.output_format,
            parameters.destination_path,
        )
        writer = get_writer(parameters.output_format, parameters.destination_path)

        with open(parameters.destination_path, "w", encoding="utf-8") as file:
            writer.write_result(transcript, file=file)

        channel.set_job_status(mcai.JobStatus.Completed)


def main():
    description = mcai.WorkerDescription(__package__)
    worker = McaiWorker(McaiWorkerParameters, description)
    worker.start()
